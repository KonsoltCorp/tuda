## tudA

# Prepare Build
execute `gulp clean copy-application` to clean the project and copy sources from www/ --> tuda-mobile/www/

# Build
execute `cd tuda-mobile/`
execute `cordova build` or `cordova build <android|ios|browser>` for a specific platform build

# Run/Deploy
execute `cordova run <browser|ios|android>`

# combined workflow after changing directory (cd) into tuda-mobile
execute `gulp && cordova build browser && cordova run browser`