var gulp = require('gulp');
var del = require('del');
var sources = ['www/**/*'];
var cordovaDest = 'tuda-mobile/www/';

gulp.task('clean', function(){
  return del.sync([cordovaDest]);
});

gulp.task('copy-application', function(){
  return gulp.src(sources).pipe(gulp.dest(cordovaDest))
});
gulp.task('default', ['clean', 'copy-application']);